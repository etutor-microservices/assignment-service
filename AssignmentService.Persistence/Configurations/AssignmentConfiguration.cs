using AssignmentService.Domain.AssignmentAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AssignmentService.Persistence.Configurations
{
    public class AssignmentConfiguration : IEntityTypeConfiguration<Assignment>
    {
        public void Configure(EntityTypeBuilder<Assignment> builder)
        {
            builder.HasKey(a => a.Id);
            builder.Property(a => a.Title).HasMaxLength(50);
            builder.Property(a => a.Description).HasMaxLength(250);
            builder.Property(a => a.SessionId).HasMaxLength(50);
            builder.HasMany(a => a.StudentAssignments);
        }
    }
}