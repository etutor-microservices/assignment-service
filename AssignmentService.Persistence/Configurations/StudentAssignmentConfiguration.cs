using AssignmentService.Domain.AssignmentAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AssignmentService.Persistence.Configurations
{
    public class StudentAssignmentConfiguration : IEntityTypeConfiguration<StudentAssignment>
    {
        public void Configure(EntityTypeBuilder<StudentAssignment> builder)
        {
            builder.HasKey(sa => sa.Id);
            builder.Property(sa => sa.StudentId).HasMaxLength(50);
            builder.Property(sa => sa.Point).HasPrecision(2);
        }
    }
}