using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace AssignmentService.Persistence.Repositories
{
    public class BaseRepository<T> : IAsyncReadRepository<T>, IAsyncRepository<T> where T : class, IAggregateRoot
    {
        protected ApplicationDbContext DbContext { get; }

        public BaseRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual async Task<List<T>> ListAsync()
        {
            return await DbContext.Set<T>().ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await DbContext.Set<T>().FindAsync(id);
        }

        public async Task<T> AddAsync(T entity)
        {
            var addedEntity = DbContext.Set<T>().Add(entity).Entity;
            await DbContext.SaveChangesAsync();
            return addedEntity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            await DbContext.SaveChangesAsync();
            return entity;
        }
    }
}