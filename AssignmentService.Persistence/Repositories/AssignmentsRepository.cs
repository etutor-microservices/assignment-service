using AssignmentService.Application.Contracts;
using AssignmentService.Domain.AssignmentAggregate;

namespace AssignmentService.Persistence.Repositories
{
    public class AssignmentsRepository : BaseRepository<Assignment>, IAssignmentsRepository
    {
        public AssignmentsRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}