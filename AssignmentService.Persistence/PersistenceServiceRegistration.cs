using AssignmentService.Application.Contracts;
using AssignmentService.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AssignmentService.Persistence
{
    public static class PersistenceServiceRegistration
    {
        public static void RegisterPersistenceService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(o =>
                o.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(IAsyncReadRepository<>), typeof(BaseRepository<>));

            services.AddScoped<IAssignmentsRepository, AssignmentsRepository>();
        }
    }
}