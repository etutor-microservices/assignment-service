using System;
using System.Threading.Tasks;
using AssignmentService.Application.Features.Assignments.Queries.GetAssignmentById;
using AssignmentService.Application.Features.StudentAssignments.Commands.AddNewStudentAssignment;
using AssignmentService.Application.Features.StudentAssignments.Commands.UpdateStudentAssignment;
using AssignmentService.Application.Features.StudentAssignments.Queries.GetStudentAssignmentDetails;
using AssignmentService.Domain.AssignmentAggregate;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AssignmentService.API.Controllers
{
    [ApiController]
    [Route("assignments/{assignment-id:guid}/student-assignments")]
    public class StudentAssignmentsController : BaseController
    {
        private readonly IMediator _mediator;

        public StudentAssignmentsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<IActionResult> GetPagedListForAssignment([FromRoute] Guid assignmentId)
        {
            var assignment = await GetAssignmentById(assignmentId);
            if (assignment == null) return NotFound($"Cannot find assignment with id {assignmentId}");

            return Ok();
        }

        [HttpGet("{id:guid}", Name = "GetStudentAssignmentDetails")]
        public async Task<IActionResult> GetDetails([FromRoute] Guid assignmentId,
            [FromRoute] GetStudentAssignmentDetails getStudentAssignmentDetails)
        {
            // TODO: call gRPC and get all files related to this student assignment
            var assignment = await GetAssignmentById(assignmentId);
            if (assignment == null) return NotFound($"Assignment with id {assignmentId} cannot be found");

            getStudentAssignmentDetails.Assignment = assignment;
            var studentAssignment = await _mediator.Send(getStudentAssignmentDetails);
            if (studentAssignment == null)
                return NotFound($"Student assignment with id {getStudentAssignmentDetails.Id} cannot be found");

            return Ok(studentAssignment);
        }

        [HttpPost]
        public async Task<IActionResult> AddNewForAssignment([FromRoute] Guid assignmentId,
            [FromBody] AddNewStudentAssignment addNewStudentAssignment)
        {
            // TODO: upload files
            var assignment = await GetAssignmentById(assignmentId);
            if (assignment == null) return NotFound($"Cannot find assignment with id {assignmentId}");

            addNewStudentAssignment.Assignment = assignment;
            var (errors, studentAssignment) = await _mediator.Send(addNewStudentAssignment);
            if (errors != null) return BadRequest(errors);

            return CreatedAtRoute("GetStudentAssignmentDetails", new {assignmentId, id = studentAssignment.Id},
                studentAssignment);
        }

        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> Update([FromRoute] Guid assignmentId,
            [FromBody] UpdateStudentAssignment updateStudentAssignment)
        {
            var assignment = await GetAssignmentById(assignmentId);
            if (assignment == null) return NotFound($"Cannot find assignment with id {assignmentId}");

            updateStudentAssignment.Assignment = assignment;
            var (found, errors, studentAssignment) = await _mediator.Send(updateStudentAssignment);

            if (!found) return NotFound($"Student assignment with id {updateStudentAssignment.Id} cannot be found");
            if (errors != null) return BadRequest(errors);

            return Ok(studentAssignment);
        }

        private async Task<Assignment> GetAssignmentById(Guid id)
        {
            var assignment = await _mediator.Send(new GetAssignmentById {Id = id});
            return assignment;
        }
    }
}