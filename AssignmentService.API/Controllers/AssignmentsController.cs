using System;
using System.Text.Json;
using System.Threading.Tasks;
using AssignmentService.Application.Features.Assignments.Commands.CreateNewAssignment;
using AssignmentService.Application.Features.Assignments.Commands.DeleteAssignment;
using AssignmentService.Application.Features.Assignments.Commands.UpdateAssignment;
using AssignmentService.Application.Features.Assignments.Queries.GetAssignmentById;
using AssignmentService.Application.Features.Assignments.Queries.GetAssignmentDetails;
using AssignmentService.Application.Features.Assignments.Queries.GetAssignmentPagedList;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AssignmentService.API.Controllers
{
    [ApiController]
    [Route("assignments")]
    public class AssignmentsController : BaseController
    {
        private readonly IMediator _mediator;

        public AssignmentsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<IActionResult> GetPagedList([FromQuery] GetAssignmentPagedList getAssignmentPagedList)
        {
            var pagedList = await _mediator.Send(getAssignmentPagedList);

            Response.Headers.Add("X-Pagination",
                JsonSerializer.Serialize(new
                {
                    pagedList.CurrentPage,
                    pagedList.PageSize,
                    pagedList.TotalCount,
                    pagedList.TotalPage
                }, new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                }));

            Response.Headers.Add("Access-Control-Expose-Headers", "X-Pagination");

            return Ok(pagedList);
        }

        [HttpGet("{id:guid}", Name = "GetAssignmentDetails")]
        public async Task<IActionResult> GetDetails([FromRoute] GetAssignmentDetails getAssignmentDetails)
        {
            var assignment = await _mediator.Send(getAssignmentDetails);
            if (assignment == null) return NotFound($"Cannot find assignment with id {getAssignmentDetails.Id}");
            return Ok(assignment);
        }

        [HttpPost]
        public async Task<IActionResult> CreateNew([FromBody] CreateNewAssignment createNewAssignment)
        {
            var (errors, assignment) = await _mediator.Send(createNewAssignment);
            if (errors != null) return BadRequest(errors);
            return CreatedAtRoute("GetAssignmentDetails", new {id = assignment.Id}, assignment);
        }

        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> Update([FromRoute] GetAssignmentById getAssignmentById,
            [FromBody] UpdateAssignment updateAssignment)
        {
            var assignment = await _mediator.Send(getAssignmentById);
            if (assignment == null) return null;

            updateAssignment.Assignment = assignment;
            var updatedAssignment = await _mediator.Send(updateAssignment);

            return Ok(updatedAssignment);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var assignment = await _mediator.Send(new GetAssignmentById {Id = id});
            if (assignment == null) return NotFound($"Cannot find assignment with id {id}");

            await _mediator.Send(new DeleteAssignment {Assignment = assignment});
            return NoContent();
        }
    }
}