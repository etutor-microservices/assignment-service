﻿using System;
using System.Collections.Generic;
using AssignmentService.Domain.Common;

namespace AssignmentService.Domain.AssignmentAggregate
{
    public class Assignment : AuditableEntity, IAggregateRoot
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        public DateTime Deadline { get; private set; }
        public string SessionId { get; private set; }

        private readonly List<StudentAssignment> _studentAssignments = new();
        public IReadOnlyList<StudentAssignment> StudentAssignments => _studentAssignments.AsReadOnly();

        public Assignment(string title, string description, DateTime deadline, string sessionId)
        {
            Title = title;
            Description = description;
            Deadline = deadline;
            SessionId = sessionId;
        }

        public void UpdateDescription(string description)
        {
            if (string.IsNullOrEmpty(description)) return;
            Description = description;
        }

        public void UpdateDeadline(DateTime deadline)
        {
            if (deadline == default) return;
            Deadline = deadline;
        }

        public void UpdateSessionId(string sessionId)
        {
            if (string.IsNullOrEmpty(sessionId)) return;
            SessionId = sessionId;
        }

        public void AddStudentAssignment(StudentAssignment studentAssignment)
        {
            _studentAssignments.Add(studentAssignment);
        }

        public void UpdateTitle(string title)
        {
            if (string.IsNullOrEmpty(title)) return;
            Title = title;
        }

        public void Delete()
        {
            DeletedAt = DateTime.UtcNow;
        }
    }
}