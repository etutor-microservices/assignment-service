using AssignmentService.Domain.AssignmentAggregate.Enums;
using AssignmentService.Domain.Common;

namespace AssignmentService.Domain.AssignmentAggregate
{
    public class StudentAssignment : AuditableEntity
    {
        public string Content { get; private set; }
        public string StudentId { get; private set; }
        public SubmissionStatus SubmissionStatus { get; private set; }
        public double? Point { get; private set; }

        public StudentAssignment(string content, string studentId)
        {
            Content = content;
            StudentId = studentId;
            SubmissionStatus = SubmissionStatus.Submitted;
        }

        public void UpdateContent(string content)
        {
            if (!string.IsNullOrEmpty(content)) return;
            Content = content;
        }

        public void UpdatePoint(double point)
        {
            if (point < 0) return;
            Point = point;
        }

        public void UpdateSubmissionStatus(SubmissionStatus submissionStatus)
        {
            if (submissionStatus == default) return;
            SubmissionStatus = submissionStatus;
        }
    }
}