namespace AssignmentService.Domain.AssignmentAggregate.Enums
{
    public enum SubmissionStatus
    {
        None,
        Submitted,
        Rejected,
        Graded
    }
}