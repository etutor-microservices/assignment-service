﻿FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["AssignmentService.API/AssignmentService.API.csproj", "AssignmentService.API/"]
COPY ["AssignmentService.Persistence/AssignmentService.Persistence.csproj", "AssignmentService.Persistence/"]
COPY ["AssignmentService.Application/AssignmentService.Application.csproj", "AssignmentService.Application/"]
COPY ["AssignmentService.Domain/AssignmentService.Domain.csproj", "AssignmentService.Domain/"]
RUN dotnet restore "AssignmentService.API/AssignmentService.API.csproj"
COPY . .
WORKDIR "/src/AssignmentService.API"
RUN dotnet build "AssignmentService.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "AssignmentService.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AssignmentService.API.dll"]
