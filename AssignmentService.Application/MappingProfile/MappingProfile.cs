using AssignmentService.Application.Features.Assignments.Queries.GetAssignmentPagedList;
using AssignmentService.Application.Features.Assignments.ViewModels;
using AssignmentService.Application.Features.StudentAssignments.ViewModels;
using AssignmentService.Domain.AssignmentAggregate;
using AutoMapper;

namespace AssignmentService.Application.MappingProfile
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Assignment, AssignmentVm>();
            CreateMap<Assignment, AssignmentDetailsVm>();

            CreateMap<StudentAssignment, StudentAssignmentVm>();
            CreateMap<StudentAssignment, StudentAssignmentDetailsVm>();
        }
    }
}