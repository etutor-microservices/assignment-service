using System.Threading.Tasks;
using AssignmentService.Domain.Common;

namespace AssignmentService.Application.Contracts
{
    public interface IAsyncRepository<T> where T : IAggregateRoot
    {
        Task<T> AddAsync(T entity);
        Task<T> UpdateAsync(T entity);
    }
}