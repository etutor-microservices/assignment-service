using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssignmentService.Domain.Common;

namespace AssignmentService.Application.Contracts
{
    public interface IAsyncReadRepository<T> where T : IAggregateRoot
    {
        Task<List<T>> ListAsync();
        Task<T> GetByIdAsync(Guid id);
    }
}