using AssignmentService.Domain.AssignmentAggregate;

namespace AssignmentService.Application.Contracts
{
    public interface IAssignmentsRepository : IAsyncRepository<Assignment>, IAsyncReadRepository<Assignment>
    {
    }
}