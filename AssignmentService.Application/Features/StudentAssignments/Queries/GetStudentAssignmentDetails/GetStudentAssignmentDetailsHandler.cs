using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Application.Features.StudentAssignments.ViewModels;
using AutoMapper;
using MediatR;

namespace AssignmentService.Application.Features.StudentAssignments.Queries.GetStudentAssignmentDetails
{
    public class
        GetStudentAssignmentDetailsHandler : IRequestHandler<GetStudentAssignmentDetails, StudentAssignmentDetailsVm>
    {
        private readonly IMapper _mapper;

        public GetStudentAssignmentDetailsHandler(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<StudentAssignmentDetailsVm> Handle(GetStudentAssignmentDetails request,
            CancellationToken cancellationToken)
        {
            var studentAssignment =
                request.Assignment.StudentAssignments.FirstOrDefault(sa => sa.Id == request.Id);

            return Task.FromResult(studentAssignment == null
                ? null
                : _mapper.Map<StudentAssignmentDetailsVm>(studentAssignment));
        }
    }
}