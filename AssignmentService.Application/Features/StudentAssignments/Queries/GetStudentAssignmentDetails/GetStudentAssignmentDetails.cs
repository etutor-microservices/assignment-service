using System;
using AssignmentService.Application.Features.StudentAssignments.ViewModels;
using AssignmentService.Domain.AssignmentAggregate;
using MediatR;

namespace AssignmentService.Application.Features.StudentAssignments.Queries.GetStudentAssignmentDetails
{
    public class GetStudentAssignmentDetails : IRequest<StudentAssignmentDetailsVm>
    {
        public Assignment Assignment { get; set; }
        public Guid Id { get; init; }
    }
}