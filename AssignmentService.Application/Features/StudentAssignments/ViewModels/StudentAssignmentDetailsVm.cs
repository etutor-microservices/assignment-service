using System;

namespace AssignmentService.Application.Features.StudentAssignments.ViewModels
{
    public class StudentAssignmentDetailsVm
    {
        public string Content { get; init; }
        public string SubmissionStatus { get; init; }
        public string StudentId { get; init; }
        public double? Point { get; init; }
        public DateTime CreatedAt { get; init; }
        public DateTime? UpdatedAt { get; init; }
    }
}