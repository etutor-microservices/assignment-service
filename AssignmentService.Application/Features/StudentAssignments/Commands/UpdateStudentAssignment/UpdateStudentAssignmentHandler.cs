using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Application.Features.StudentAssignments.ViewModels;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace AssignmentService.Application.Features.StudentAssignments.Commands.UpdateStudentAssignment
{
    public class UpdateStudentAssignmentHandler : IRequestHandler<UpdateStudentAssignment, (bool found,
        List<ValidationFailure> errors, StudentAssignmentDetailsVm studentAssignment)>
    {
        private readonly IAssignmentsRepository _assignmentsRepository;
        private readonly IValidator<UpdateStudentAssignment> _validator;
        private readonly IMapper _mapper;

        public UpdateStudentAssignmentHandler(IAssignmentsRepository assignmentsRepository,
            IValidator<UpdateStudentAssignment> validator, IMapper mapper)
        {
            _assignmentsRepository =
                assignmentsRepository ?? throw new ArgumentNullException(nameof(assignmentsRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(bool found, List<ValidationFailure>errors, StudentAssignmentDetailsVm studentAssignment)>
            Handle(UpdateStudentAssignment request, CancellationToken cancellationToken)
        {
            var studentAssignment = request.Assignment.StudentAssignments.FirstOrDefault(sa => sa.Id == request.Id);
            if (studentAssignment == null) return (false, null, null);

            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (true, validationResult.Errors, null);

            studentAssignment.UpdatePoint(request.Point);
            studentAssignment.UpdateSubmissionStatus(request.SubmissionStatus);
            studentAssignment.UpdateContent(request.Content);

            await _assignmentsRepository.UpdateAsync(request.Assignment);

            return (true, null, _mapper.Map<StudentAssignmentDetailsVm>(studentAssignment));
        }
    }
}