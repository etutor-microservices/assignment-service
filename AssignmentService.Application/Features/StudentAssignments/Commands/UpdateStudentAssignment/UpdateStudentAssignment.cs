using System;
using System.Collections.Generic;
using AssignmentService.Application.Features.StudentAssignments.ViewModels;
using AssignmentService.Domain.AssignmentAggregate;
using AssignmentService.Domain.AssignmentAggregate.Enums;
using FluentValidation.Results;
using MediatR;

namespace AssignmentService.Application.Features.StudentAssignments.Commands.UpdateStudentAssignment
{
    public class
        UpdateStudentAssignment :
            IRequest<(bool found, List<ValidationFailure> errors, StudentAssignmentDetailsVm studentAssignment)>
    {
        public Assignment Assignment { get; set; }
        public Guid Id { get; init; }
        public string Content { get; init; }
        public SubmissionStatus SubmissionStatus { get; init; }
        public double Point { get; init; }
    }
}