using System.Linq;
using FluentValidation;

namespace AssignmentService.Application.Features.StudentAssignments.Commands.UpdateStudentAssignment
{
    public class UpdateStudentAssignmentValidator : AbstractValidator<UpdateStudentAssignment>
    {
        public UpdateStudentAssignmentValidator()
        {
            RuleFor(usa => usa.Content).MaximumLength(250).When(usa => !string.IsNullOrEmpty(usa.Content));
            RuleFor(usa => usa.Point).GreaterThanOrEqualTo(0).LessThanOrEqualTo(10);
            RuleFor(usa => usa.SubmissionStatus).IsInEnum().When(usa => usa.SubmissionStatus != default);
        }
    }
}