using System.Collections.Generic;
using AssignmentService.Application.Features.StudentAssignments.ViewModels;
using AssignmentService.Domain.AssignmentAggregate;
using FluentValidation.Results;
using MediatR;

namespace AssignmentService.Application.Features.StudentAssignments.Commands.AddNewStudentAssignment
{
    public class
        AddNewStudentAssignment : IRequest<(List<ValidationFailure> errors, StudentAssignmentVm studentAssignmentVm)>
    {
        public Assignment Assignment { get; set; }
        public string Content { get; init; }
        public string StudentId { get; init; }
    }
}