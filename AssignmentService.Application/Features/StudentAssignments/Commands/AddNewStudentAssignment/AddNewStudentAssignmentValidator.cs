using System.Linq;
using FluentValidation;

namespace AssignmentService.Application.Features.StudentAssignments.Commands.AddNewStudentAssignment
{
    public class AddNewStudentAssignmentValidator : AbstractValidator<AddNewStudentAssignment>
    {
        public AddNewStudentAssignmentValidator()
        {
            RuleFor(sa => sa.Assignment.StudentAssignments.FirstOrDefault(e => e.StudentId == sa.StudentId)).Null()
                .WithMessage("Student has already submitted for this assignment.");
            RuleFor(sa => sa.StudentId).MaximumLength(50);
            RuleFor(sa => sa.Content).MaximumLength(250);
        }
    }
}