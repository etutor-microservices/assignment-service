using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Application.Features.StudentAssignments.ViewModels;
using AssignmentService.Domain.AssignmentAggregate;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace AssignmentService.Application.Features.StudentAssignments.Commands.AddNewStudentAssignment
{
    public class AddNewStudentAssignmentHandler : IRequestHandler<AddNewStudentAssignment, (List<ValidationFailure>
        errors, StudentAssignmentVm studentAssignmentVm)>
    {
        private readonly IAssignmentsRepository _assignmentsRepository;
        private readonly IValidator<AddNewStudentAssignment> _validator;
        private readonly IMapper _mapper;

        public AddNewStudentAssignmentHandler(IAssignmentsRepository assignmentsRepository,
            IValidator<AddNewStudentAssignment> validator, IMapper mapper)
        {
            _assignmentsRepository =
                assignmentsRepository ?? throw new ArgumentNullException(nameof(assignmentsRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(List<ValidationFailure> errors, StudentAssignmentVm studentAssignmentVm)> Handle(
            AddNewStudentAssignment request, CancellationToken cancellationToken)
        {
            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (validationResult.Errors, null);

            var studentAssignment = new StudentAssignment(request.Content, request.StudentId);
            request.Assignment.AddStudentAssignment(studentAssignment);

            await _assignmentsRepository.UpdateAsync(request.Assignment);
            return (null, _mapper.Map<StudentAssignmentVm>(studentAssignment));
        }
    }
}