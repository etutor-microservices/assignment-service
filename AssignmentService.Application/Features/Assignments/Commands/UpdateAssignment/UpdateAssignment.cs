using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using AssignmentService.Application.Features.Assignments.ViewModels;
using AssignmentService.Domain.AssignmentAggregate;
using FluentValidation.Results;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Commands.UpdateAssignment
{
    public class UpdateAssignment : IRequest<(List<ValidationFailure> errors, AssignmentDetailsVm assignment)>
    {
        public Assignment Assignment { get; set; }
        public string Title { get; init; }
        public string Description { get; init; }
        public DateTime Deadline { get; init; }
        public string SessionId { get; init; }
    }
}