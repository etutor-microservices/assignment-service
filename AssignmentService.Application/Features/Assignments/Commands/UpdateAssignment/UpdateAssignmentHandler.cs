using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Application.Features.Assignments.ViewModels;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Commands.UpdateAssignment
{
    public class UpdateAssignmentHandler : IRequestHandler<UpdateAssignment, (List<ValidationFailure> errors,
        AssignmentDetailsVm
        assignment)>
    {
        private readonly IAssignmentsRepository _assignmentsRepository;
        private readonly IValidator<UpdateAssignment> _validator;
        private readonly IMapper _mapper;

        public UpdateAssignmentHandler(IAssignmentsRepository assignmentsRepository,
            IValidator<UpdateAssignment> validator, IMapper mapper)
        {
            _assignmentsRepository =
                assignmentsRepository ?? throw new ArgumentNullException(nameof(assignmentsRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(List<ValidationFailure> errors, AssignmentDetailsVm assignment)> Handle(
            UpdateAssignment request, CancellationToken cancellationToken)
        {
            var validator = new UpdateAssignmentValidator();
            var validationResult = await validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (validationResult.Errors, null);

            request.Assignment.UpdateTitle(request.Title);
            request.Assignment.UpdateDescription(request.Description);
            request.Assignment.UpdateDeadline(request.Deadline);
            request.Assignment.UpdateSessionId(request.SessionId);

            await _assignmentsRepository.UpdateAsync(request.Assignment);

            return (null, _mapper.Map<AssignmentDetailsVm>(request.Assignment));
        }
    }
}