using FluentValidation;

namespace AssignmentService.Application.Features.Assignments.Commands.UpdateAssignment
{
    public class UpdateAssignmentValidator : AbstractValidator<UpdateAssignment>
    {
        public UpdateAssignmentValidator()
        {
            RuleFor(u => u.Title).MaximumLength(50).When(u => !string.IsNullOrEmpty(u.Title));
            RuleFor(u => u.Description).MaximumLength(250).When(u => !string.IsNullOrEmpty(u.Description));
            RuleFor(u => u.Deadline).GreaterThan(u => u.Assignment.Deadline).When(u => u.Deadline != default);
            RuleFor(u => u.SessionId).MaximumLength(50).When(u => !string.IsNullOrEmpty(u.SessionId));
        }
    }
}