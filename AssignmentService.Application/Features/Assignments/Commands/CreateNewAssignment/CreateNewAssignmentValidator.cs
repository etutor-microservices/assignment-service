using System;
using FluentValidation;

namespace AssignmentService.Application.Features.Assignments.Commands.CreateNewAssignment
{
    public class CreateNewAssignmentValidator : AbstractValidator<CreateNewAssignment>
    {
        public CreateNewAssignmentValidator()
        {
            RuleFor(c => c.Title).MaximumLength(50);
            RuleFor(c => c.Description).MaximumLength(250);
            RuleFor(c => c.SessionId).NotEmpty().NotNull().MaximumLength(50);
            RuleFor(c => c.Deadline).NotNull().GreaterThan(DateTime.UtcNow);
        }
    }
}