using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Domain.AssignmentAggregate;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Commands.CreateNewAssignment
{
    public class CreateNewAssignmentHandler : IRequestHandler<CreateNewAssignment, (List<ValidationFailure> errors,
        Assignment assignment)>
    {
        private readonly IAssignmentsRepository _assignmentsRepository;
        private readonly IValidator<CreateNewAssignment> _validator;

        public CreateNewAssignmentHandler(IAssignmentsRepository assignmentsRepository,
            IValidator<CreateNewAssignment> validator)
        {
            _assignmentsRepository =
                assignmentsRepository ?? throw new ArgumentNullException(nameof(assignmentsRepository));
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
        }

        public async Task<(List<ValidationFailure> errors, Assignment assignment)> Handle(CreateNewAssignment request,
            CancellationToken cancellationToken)
        {
            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid) return (validationResult.Errors, null);

            var assignment = new Assignment(request.Title, request.Description, request.Deadline, request.SessionId);

            return (null, await _assignmentsRepository.AddAsync(assignment));
        }
    }
}