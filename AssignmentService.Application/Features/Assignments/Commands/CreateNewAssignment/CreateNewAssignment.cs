using System;
using System.Collections.Generic;
using AssignmentService.Domain.AssignmentAggregate;
using FluentValidation.Results;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Commands.CreateNewAssignment
{
    public class CreateNewAssignment : IRequest<(List<ValidationFailure> errors, Assignment assignment)>
    {
        public string Title { get; set; }
        public string Description { get; init; }
        public DateTime Deadline { get; init; }
        public string SessionId { get; init; }
    }
}