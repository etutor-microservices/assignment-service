using System;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Commands.DeleteAssignment
{
    public class DeleteAssignmentHandler : IRequestHandler<DeleteAssignment>
    {
        private readonly IAssignmentsRepository _assignmentsRepository;

        public DeleteAssignmentHandler(IAssignmentsRepository assignmentsRepository)
        {
            _assignmentsRepository =
                assignmentsRepository ?? throw new ArgumentNullException(nameof(assignmentsRepository));
        }

        public async Task<Unit> Handle(DeleteAssignment request, CancellationToken cancellationToken)
        {
            request.Assignment.Delete();

            await _assignmentsRepository.UpdateAsync(request.Assignment);

            return Unit.Value;
        }
    }
}