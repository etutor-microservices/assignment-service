using AssignmentService.Domain.AssignmentAggregate;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Commands.DeleteAssignment
{
    public class DeleteAssignment : IRequest
    {
        public Assignment Assignment { get; init; }
    }
}