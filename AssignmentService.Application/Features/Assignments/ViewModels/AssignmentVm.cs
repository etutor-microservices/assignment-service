using System;

namespace AssignmentService.Application.Features.Assignments.ViewModels
{
    public class AssignmentVm
    {
        public string Title { get; init; }
        public DateTime CreatedAt { get; set; }
    }
}