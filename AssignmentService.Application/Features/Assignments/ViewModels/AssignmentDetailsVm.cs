using System;
using System.Collections.Generic;
using AssignmentService.Application.Features.StudentAssignments.ViewModels;

namespace AssignmentService.Application.Features.Assignments.ViewModels
{
    public class AssignmentDetailsVm
    {
        public string Title { get; init; }
        public string Description { get; init; }
        public DateTime Deadline { get; init; }
        public string SessionId { get; init; }
        public IEnumerable<StudentAssignmentVm> StudentAssignments { get; init; }
        public DateTime CreatedAt { get; init; }
        public DateTime? UpdatedAt { get; init; }
    }
}