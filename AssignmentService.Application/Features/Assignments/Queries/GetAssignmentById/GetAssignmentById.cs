using System;
using AssignmentService.Domain.AssignmentAggregate;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Queries.GetAssignmentById
{
    public class GetAssignmentById : IRequest<Assignment>
    {
        public Guid Id { get; init; }
    }
}