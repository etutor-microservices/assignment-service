using System;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Domain.AssignmentAggregate;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Queries.GetAssignmentById
{
    public class GetAssignmentByIdHandler : IRequestHandler<GetAssignmentById, Assignment>
    {
        private readonly IAssignmentsRepository _assignmentsRepository;

        public GetAssignmentByIdHandler(IAssignmentsRepository assignmentsRepository)
        {
            _assignmentsRepository =
                assignmentsRepository ?? throw new ArgumentNullException(nameof(assignmentsRepository));
        }

        public async Task<Assignment> Handle(GetAssignmentById request, CancellationToken cancellationToken)
        {
            return await _assignmentsRepository.GetByIdAsync(request.Id);
        }
    }
}