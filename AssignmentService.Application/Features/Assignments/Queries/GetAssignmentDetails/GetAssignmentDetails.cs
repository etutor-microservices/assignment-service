using System;
using AssignmentService.Application.Features.Assignments.Queries.GetAssignmentById;
using AssignmentService.Application.Features.Assignments.ViewModels;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Queries.GetAssignmentDetails
{
    public class GetAssignmentDetails : IRequest<AssignmentDetailsVm>
    {
        public Guid Id { get; init; }
    }
}