using System;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Application.Features.Assignments.Queries.GetAssignmentById;
using AssignmentService.Application.Features.Assignments.ViewModels;
using AutoMapper;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Queries.GetAssignmentDetails
{
    public class GetAssignmentDetailsHandler : IRequestHandler<GetAssignmentDetails, AssignmentDetailsVm>
    {
        private readonly IAssignmentsRepository _assignmentsRepository;
        private readonly IMapper _mapper;

        public GetAssignmentDetailsHandler(IAssignmentsRepository assignmentsRepository, IMapper mapper)
        {
            _assignmentsRepository =
                assignmentsRepository ?? throw new ArgumentNullException(nameof(assignmentsRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<AssignmentDetailsVm> Handle(GetAssignmentDetails request, CancellationToken cancellationToken)
        {
            var assignment = await _assignmentsRepository.GetByIdAsync(request.Id);
            if (assignment == null) return null;

            var assignmentDetailsVm = _mapper.Map<AssignmentDetailsVm>(assignment);
            return assignmentDetailsVm;
        }
    }
}