using AssignmentService.Application.Features.Assignments.ViewModels;
using AssignmentService.Application.Shared.Requests;
using AssignmentService.Application.Shared.Responses;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Queries.GetAssignmentPagedList
{
    public class GetAssignmentPagedList : GetPagedList, IRequest<PagedList<AssignmentVm>>
    {
    }
}