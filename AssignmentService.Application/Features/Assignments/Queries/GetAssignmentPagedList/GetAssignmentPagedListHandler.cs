using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Application.Features.Assignments.ViewModels;
using AssignmentService.Application.Shared.Responses;
using AutoMapper;
using MediatR;

namespace AssignmentService.Application.Features.Assignments.Queries.GetAssignmentPagedList
{
    public class
        GetAssignmentPagedListHandler : IRequestHandler<GetAssignmentPagedList, PagedList<AssignmentVm>>
    {
        private readonly IAssignmentsRepository _assignmentsRepository;
        private readonly IMapper _mapper;

        public GetAssignmentPagedListHandler(IAssignmentsRepository assignmentsRepository, IMapper mapper)
        {
            _assignmentsRepository =
                assignmentsRepository ?? throw new ArgumentNullException(nameof(assignmentsRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<PagedList<AssignmentVm>> Handle(GetAssignmentPagedList request,
            CancellationToken cancellationToken)
        {
            var assignmentList = await _assignmentsRepository.ListAsync();

            var assignmentVmList = _mapper.Map<List<AssignmentVm>>(assignmentList);

            var assignmentPagedList =
                new PagedList<AssignmentVm>(assignmentVmList, request.PageNumber, request.PageSize);

            return assignmentPagedList;
        }
    }
}