using System;
using AssignmentService.Domain.AssignmentAggregate;
using AssignmentService.Domain.AssignmentAggregate.Enums;
using FluentAssertions;
using Xunit;

namespace AssignmentService.UnitTesting.Domain
{
    public class StudentAssignmentUnitTesting
    {
        private readonly StudentAssignment _studentAssignment = new("content", Guid.NewGuid().ToString());

        [Fact]
        public void Creation_ShouldReturnExpectedStudentAssignment()
        {
            var content = "content";
            var studentId = Guid.NewGuid().ToString();

            var studentAssignment = new StudentAssignment(content, studentId);

            studentAssignment.Content.Should().Be(content);
            studentAssignment.SubmissionStatus.Should().Be(SubmissionStatus.Submitted);
            studentAssignment.CreatedAt.Should().BeBefore(DateTime.UtcNow);
            studentAssignment.UpdatedAt.Should().BeNull();
            studentAssignment.DeletedAt.Should().BeNull();
        }

        [Fact]
        public void UpdatePoint_ShouldReturnGradedStudentAssignment()
        {
            var point = 8.5;

            _studentAssignment.UpdatePoint(point);

            _studentAssignment.Point.Should().Be(point);
        }

        [Fact]
        public void UpdatePoint_NegativePoint_ShouldNotUpdatePoint()
        {
            var point = -8.5;

            _studentAssignment.UpdatePoint(point);

            _studentAssignment.Point.Should().NotBe(point);
        }

        [Fact]
        public void UpdateSubmissionStatus_ShouldReturnUpdatedStudentAssignment()
        {
            _studentAssignment.UpdateSubmissionStatus(SubmissionStatus.Graded);

            _studentAssignment.SubmissionStatus.Should().Be(SubmissionStatus.Graded);
        }

        [Fact]
        public void UpdateSubmissionStatus_InvalidStatus_ShouldNotUpdateStudentAssignment()
        {
            _studentAssignment.UpdateSubmissionStatus(SubmissionStatus.None);

            _studentAssignment.SubmissionStatus.Should().NotBe(SubmissionStatus.None);
        }
    }
}