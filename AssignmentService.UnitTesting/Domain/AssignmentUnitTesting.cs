using System;
using System.Linq;
using AssignmentService.Domain.AssignmentAggregate;
using FluentAssertions;
using Xunit;

namespace AssignmentService.UnitTesting.Domain
{
    public class AssignmentUnitTesting
    {
        private readonly Assignment _assignment =
            new("Title", "Description", DateTime.UtcNow.AddDays(2), Guid.NewGuid().ToString());

        [Fact]
        public void Creation_ShouldReturnExpectedAssignment()
        {
            var title = "Title";
            var description = "Description";
            var deadline = DateTime.UtcNow.AddDays(2);
            var sessionId = Guid.NewGuid().ToString();

            var assignment = new Assignment(title, description, deadline, sessionId);

            assignment.Title.Should().Be(title);
            assignment.Description.Should().Be(description);
            assignment.Deadline.Should().Be(deadline);
            assignment.SessionId.Should().Be(sessionId);
            assignment.StudentAssignments.Should().BeEmpty();
        }

        [Fact]
        public void UpdateTitle_ShouldReturnUpdatedAssignment()
        {
            var title = "Updated Title";

            _assignment.UpdateTitle(title);

            _assignment.Title.Should().Be(title);
        }

        [Fact]
        public void UpdateTitle_NullTitle_ShouldNotUpdateAssignmentTitle()
        {
            var title = string.Empty;

            _assignment.UpdateTitle(title);

            _assignment.Title.Should().NotBe(title);
        }

        [Fact]
        public void UpdateDescription_ShouldReturnUpdatedAssignment()
        {
            var updatedDescription = "updated description";

            _assignment.UpdateDescription(updatedDescription);

            _assignment.Description.Should().Be(updatedDescription);
        }

        [Fact]
        public void UpdateDescription_InvalidParam_ShouldNotUpdateAssignment()
        {
            var updatedDescription = "";

            _assignment.UpdateDescription(updatedDescription);

            _assignment.Description.Should().NotBe(updatedDescription);
        }

        [Fact]
        public void UpdateDeadline_ShouldReturnUpdatedAssignment()
        {
            var newDeadline = DateTime.UtcNow.AddDays(7);

            _assignment.UpdateDeadline(newDeadline);

            _assignment.Deadline.Should().Be(newDeadline);
        }

        [Fact]
        public void UpdateDeadline_InvalidParam_ShouldNotUpdateAssignment()
        {
            var newDeadline = (DateTime) default;

            _assignment.UpdateDeadline(newDeadline);

            _assignment.Deadline.Should().NotBe(newDeadline);
        }

        [Fact]
        public void UpdateSessionId_ShouldReturnUpdatedAssignment()
        {
            var updatedSessionId = Guid.NewGuid().ToString();

            _assignment.UpdateSessionId(updatedSessionId);

            _assignment.SessionId.Should().Be(updatedSessionId);
        }

        [Fact]
        public void UpdateSessionId_InvalidParam_ShouldNotUpdateAssignment()
        {
            var updatedSessionId = string.Empty;

            _assignment.UpdateSessionId(updatedSessionId);

            _assignment.SessionId.Should().NotBe(updatedSessionId);
        }

        [Fact]
        public void AddStudentAssignment_ShouldAddStudentAssignment()
        {
            var studentAssignment = new StudentAssignment("content", Guid.NewGuid().ToString());

            _assignment.AddStudentAssignment(studentAssignment);

            _assignment.StudentAssignments.Count.Should().Be(1);
            _assignment.StudentAssignments.First().Should().Be(studentAssignment);
        }

        [Fact]
        public void Delete_ShouldSetDeletedAt()
        {
            _assignment.Delete();

            _assignment.DeletedAt.Should().NotBeNull();
        }
    }
}