using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Application.Features.Assignments.Queries.GetAssignmentPagedList;
using AssignmentService.Application.MappingProfile;
using AssignmentService.Domain.AssignmentAggregate;
using AutoMapper;
using FluentAssertions;
using Moq;
using Xunit;

namespace AssignmentService.UnitTesting.Application.Features.Assignments
{
    public class GetAssignmentPagesListUnitTesting
    {
        private readonly Mock<IAssignmentsRepository> _mockAssignmentsRepository;
        private readonly IMapper _mapper;

        private readonly List<Assignment> _assignments = new();

        public GetAssignmentPagesListUnitTesting()
        {
            _mockAssignmentsRepository = new Mock<IAssignmentsRepository>();
            _mapper = new MapperConfiguration(cfg => { cfg.AddProfile<MappingProfile>(); }).CreateMapper();

            for (var i = 0; i < 20; i++)
                _assignments.Add(new Assignment($"Title {i}", $"Description {i}", DateTime.UtcNow.AddDays(i),
                    Guid.NewGuid().ToString()));
        }

        [Fact]
        public async Task GetPagedList_ShouldReturnExpectedPagedList()
        {
            var request = new GetAssignmentPagedList()
            {
                PageNumber = 1,
                PageSize = 10
            };

            _mockAssignmentsRepository.Setup(x => x.ListAsync()).ReturnsAsync(_assignments);

            var handler = new GetAssignmentPagedListHandler(_mockAssignmentsRepository.Object, _mapper);
            var response = await handler.Handle(request, default);

            response.Count.Should().Be(request.PageSize);
            response.CurrentPage.Should().Be(request.PageNumber);
            response.PageSize.Should().Be(request.PageSize);
            response.TotalCount.Should().Be(_assignments.Count);
            response.TotalPage.Should().Be((int) Math.Ceiling(_assignments.Count / (double) request.PageSize));
        }

        [Fact]
        public async Task GetPagedList_InExistentPage_ShouldReturnEmptyList()
        {
            var pageSize = 10;

            var request = new GetAssignmentPagedList()
            {
                PageNumber = (int) Math.Ceiling(_assignments.Count / (double) pageSize) + 1,
                PageSize = pageSize
            };

            _mockAssignmentsRepository.Setup(x => x.ListAsync()).ReturnsAsync(_assignments);

            var handler = new GetAssignmentPagedListHandler(_mockAssignmentsRepository.Object, _mapper);
            var response = await handler.Handle(request, default);

            response.Should().BeEmpty();
            response.CurrentPage.Should().Be(request.PageNumber);
            response.PageSize.Should().Be(request.PageSize);
            response.TotalCount.Should().Be(_assignments.Count);
            response.TotalPage.Should().Be((int) Math.Ceiling(_assignments.Count / (double) request.PageSize));
        }
    }
}