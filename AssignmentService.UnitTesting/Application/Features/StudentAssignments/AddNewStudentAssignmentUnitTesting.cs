using System;
using System.Linq;
using System.Threading.Tasks;
using AssignmentService.Application.Contracts;
using AssignmentService.Application.Features.StudentAssignments.Commands.AddNewStudentAssignment;
using AssignmentService.Application.MappingProfile;
using AssignmentService.Domain.AssignmentAggregate;
using AssignmentService.Domain.AssignmentAggregate.Enums;
using AutoMapper;
using FluentAssertions;
using Moq;
using Xunit;

namespace AssignmentService.UnitTesting.Application.Features.StudentAssignments
{
    public class AddNewStudentAssignmentUnitTesting
    {
        private readonly Mock<IAssignmentsRepository> _mockAssignmentsRepository;
        private readonly IMapper _mapper;
        private readonly AddNewStudentAssignmentValidator _validator;

        private readonly Assignment _assignment =
            new("Title", "Description", DateTime.UtcNow.AddDays(2), Guid.NewGuid().ToString());


        public AddNewStudentAssignmentUnitTesting()
        {
            _mockAssignmentsRepository = new Mock<IAssignmentsRepository>();
            _validator = new AddNewStudentAssignmentValidator();
            _mapper = new MapperConfiguration(cfg => { cfg.AddProfile<MappingProfile>(); }).CreateMapper();
        }

        [Fact]
        public async Task AddNew_ShouldReturnExpectedList()
        {
            var request = new AddNewStudentAssignment
            {
                Assignment = _assignment,
                Content = "Content",
                StudentId = Guid.NewGuid().ToString()
            };

            _mockAssignmentsRepository.Setup(x => x.UpdateAsync(It.IsAny<Assignment>())).ReturnsAsync(_assignment);
            var handler = new AddNewStudentAssignmentHandler(_mockAssignmentsRepository.Object, _validator, _mapper);

            var (errors, studentAssignment) = await handler.Handle(request, default);

            errors.Should().BeNull();
            studentAssignment.Content.Should().Be(request.Content);
            studentAssignment.StudentId.Should().Be(request.StudentId);
            studentAssignment.Point.Should().BeNull();
            studentAssignment.SubmissionStatus.Should().Be(SubmissionStatus.Submitted.ToString());
        }

        [Fact]
        public async Task AddNew_DuplicateStudentAssignment_ShouldReturnErrors()
        {
            var studentId = Guid.NewGuid().ToString();
            _assignment.AddStudentAssignment(new StudentAssignment("Content", studentId));
            var request = new AddNewStudentAssignment
            {
                Assignment = _assignment,
                Content = "Content",
                StudentId = studentId
            };

            _mockAssignmentsRepository.Setup(x => x.UpdateAsync(It.IsAny<Assignment>())).ReturnsAsync(_assignment);
            var handler = new AddNewStudentAssignmentHandler(_mockAssignmentsRepository.Object, _validator, _mapper);

            var (errors, studentAssignment) = await handler.Handle(request, default);

            errors.Should().NotBeNull();
            errors.FirstOrDefault()?.ErrorMessage.Should().Be("Student has already submitted for this assignment.");
            studentAssignment.Should().BeNull();
        }
    }
}